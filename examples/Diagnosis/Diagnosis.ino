//nicht entfernen
#include <Robot.h>

void setup()
{
  initAll();
  Serial.println("------ Blechdose Test and Calibration Program ------");
  Serial.println("(C) J. Boehmer 2015");
  Serial.println("");
}


void loop()
{
  Serial.print("Current SN: ");
  Serial.println(getSN());
  Serial.print("Measured Battery voltage: ");
  Serial.println(readVcc());
  Serial.println("");
  Serial.println("Choose: ");
  Serial.println("1: Set SN");
  Serial.println("2: Set Voltage calibration");
  Serial.println("3: Test all Peripherals");
  Serial.println("4: Test LED");
  Serial.println("5: Test Motos");
  Serial.println("6: Test Button");
  Serial.println("7: Calibrate Line Sensors");
  Serial.println("8: Test line sensors");
  
  long tmp = 0;
  while (!tmp)
  {
    tmp = Serial.parseInt();
  }
  
  switch (tmp)
  {
  case 1:
    option_SN();
    break;
  case 2:
    option_volt();
    break;
  case 3:
    option_test_all();
    break;
  case 4:
    test_led();
    break;
  case 5:
    test_motos();
    break;
  case 6:
    test_button();
    break;
  case 7:
    cal_line();
    break;
  case 8:
    test_line();
    break;
    
  default:
    Serial.println("Invalid Input");
  }
  
  Serial.println("----------------------");
  Serial.println("");

}





void option_SN()
{
  Serial.print("Current SN: ");
  Serial.println(getSN());
  Serial.println("Input new SN:");
  
  long tmp = 0;
  while (!tmp)
  { 
    tmp = Serial.parseInt();
  }
   

  if (tmp > 0 && tmp < 255)
  {
    setSN(tmp);
  }
  else
  {
    Serial.println("Invalid SN-Input (must be between 0 and 255)");
  }
}

void option_volt()
{
  Serial.print("Measured Battery voltage (without correction): ");
  long volt_m = readVcc_real();
  Serial.println(volt_m);
  Serial.print("Calibration byte: ");
  Serial.println(getCal());
  Serial.println();
  Serial.println("Input real voltage in mV:");
  long volt_r = 0;
  while (!volt_r)
  {
    volt_r = Serial.parseInt();
  }
  if (volt_r > 0)
  {
    uint8_t cal = ((volt_r * 100) / volt_m)+1;
    setCal(cal);
  }
  else
  {
    Serial.println("Invalid Input");
  }
  
}

long readVcc_real() {
  long result;
  // Read 1.1V reference against AVcc
  ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  delay(2);             // Wait for Vref to settle
  ADCSRA |= _BV(ADSC);        // Convert
  while (bit_is_set(ADCSRA, ADSC));
  result = ADCL;
  result |= ADCH << 8;
  result = 1126400L / result;     // Back-calculate AVcc in mV
  return result;
}

void option_test_all()
{
  test_led();
  test_motos();
  test_button();
}

void test_led()
{
  Serial.println("LED should blink now");
  while (Serial.read()==-1)
  {
    Serial.parseInt();
    ledOn();
    delay(500);
    ledOff();
    delay(500);
  }
}

void test_button()
{
  getBtn(0,100);
  clearBtn();
  Serial.println("Press Button");
  while (Serial.read()==-1)
  {
    Serial.parseInt();  
  }
  if(getBtn(0,100)>0)
  {
    Serial.print("Taster wurde ");
    Serial.print(getBtn(0,100)+1);
    Serial.println("x gedrueckt");
  }
  else
  {
    Serial.println("Kein Tastendruck registriert");
  }
}

void test_motos()
{
  Serial.println("Left motor should drive foreward");
  while (Serial.read()==-1)
  {
    Serial.parseInt();
    motoL(255);
  }
  motoL(0);
  Serial.println("Left motor should drive backward");
  while (Serial.read()==-1)
  {
    Serial.parseInt();
    motoL(-255);
  }
  motoL(0);
  Serial.println("Right motor should drive foreward");
  while (Serial.read()==-1)
  {
    Serial.parseInt();
    motoR(255);
  }
  motoR(0);
  Serial.println("Left motor should drive backward");
  while (Serial.read()==-1)
  {
    Serial.parseInt();
    motoR(-255);
  }
  motoR(0);
}


void cal_line()
{
  //left sensor
  Serial.println("Put left sensor on white");
  while (Serial.read()==-1){}
  uint16_t white = lineLRaw();
  Serial.println("Put left sensor on black");
  while (Serial.read()==-1){}
  uint16_t black = lineLRaw();
  uint16_t l=(black+white)/2;

  //center
  Serial.println("Put center sensor on white");
  while (Serial.read()==-1){}
  white = lineCRaw();
  Serial.println("Put center sensor on black");
  while (Serial.read()==-1){}
  black = lineCRaw();
  uint16_t c=(black+white)/2;

  //right
  Serial.println("Put right sensor on white");
  while (Serial.read()==-1){}
  white = lineRRaw();
  Serial.println("Put right sensor on black");
  while (Serial.read()==-1){}
  black = lineRRaw();
  uint16_t r=(black+white)/2;
  
  setThreshold(l,c,r);
}

void test_line()
{
  while (Serial.read()==-1){
    if(getLine()==LEFT)
    {
      Serial.println("Left");
    }
    else if(getLine()==CENTER)
    {
      Serial.println("Center");
    }
    else if(getLine()==RIGHT)
    {
      Serial.println("RIGHT");
    }
    else
    {
      Serial.println("Error");
    }
    Serial.print("left thr: ");
    Serial.println(thr_left);
    Serial.print("left now: ");
    Serial.println(lineLRaw());
    Serial.print("center thr: ");
    Serial.println(thr_center);
    Serial.print("center now: ");
    Serial.println(lineCRaw());
    
    Serial.print("right thr: ");
    Serial.println(thr_right);
    Serial.print("right now: ");
    Serial.println(lineRRaw());


    delay(1000);
    }
}

