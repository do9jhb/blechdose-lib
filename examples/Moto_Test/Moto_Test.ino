//nicht entfernen
#include <Robot.h>

void setup() {
    //nicht entfernen:
    initAll();

    //hier kann weiterer Code folgen der einmal ausgefuehrt werden soll:
}

void loop() {
  // hier den Code einfuegen der immer wiederholt werden soll:
  motoL(255);
  motoR(255);

  delay(5000);

  motoL(-255);
  motoR(-255);

  delay(5000);

}