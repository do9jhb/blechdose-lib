//nicht entfernen
#include <Robot.h>

#define SPEED 220
#define ROT_SP  SPEED //Rotation speed

void setup() {
    //nicht entfernen:
    initAll();
    moto(SPEED,SPEED);
    //hier kann weiterer Code folgen der einmal ausgefuehrt werden soll:
}

void loop() { //Code hierdrinnen wird endlos wiederholt
     switch(getLine())
     {
      case CENTER:
        moto(SPEED,SPEED);
      break;
      
      case LEFT:
        moto(-ROT_SP,ROT_SP);
      break;
      
      case RIGHT:
        moto(ROT_SP,-ROT_SP);
      break;  
     }
     
}