// 
// 
// 

#include "Robot.h"

uint8_t btn_val = 0;
uint8_t btn_max = 1;
uint8_t btn_min = 0;

uint8_t cal_volt = 100;

uint16_t thr_left = 512;
uint16_t thr_right = 512;
uint16_t thr_center = 512;

#pragma region Inits

/**
 * @fn	void initAll()
 *
 * @brief	Initialisiert alle Hardwarekomponenten und gibt die Seriennummer auf der seriellen Schnittstelle aus aus.
 *
 * @author	J. Boehmer
 * @date	25.03.2015
 */
void initAll()
{
//#ifndef NO_SERIAL
	Serial.begin(BAUD);
	Serial.println("Blechdose Startup");
	Serial.print("SN: ");
	Serial.println(getSN());
//#endif
	motoInit();
	initUser();
#ifndef NO_BATT_CHECK
	checkBatt();
#endif
	//init_rf();
	initLine();

#ifndef NO_INT					//When NO_INT is defined, no Interrupt will be configured
	//Timer2 init
	TCCR2A = 0x00;				//disconnect Timerpins
	TCCR2B = 0x07;				//prescaler 1024 (15,625kHz) 
	TIMSK2 = 0x01;				//activate overflow interrupt
	sei();
#endif

	attachInterrupt(1, btnHandler, FALLING);
	//sleep(SLEEPTIME);
}

#ifndef NO_checkBatt
void checkBatt()
{
	long volt = readVcc();
#ifndef NO_SERIAL
	Serial.print("Batteriespannung (mV): ");
	Serial.println(volt);
#endif

	if (!digitalRead(BUTTON))
	{
		if (volt > 3000 && volt <= 4000)
		{
			int tmp = volt - 3000;
			tmp = tmp / 100;
			for (uint8_t n = 0; n < tmp; n++)
			{
				ledOn();
				delay(250);
				ledOff();
				delay(250);

			}
		}
	}

	if (volt <= BATTERY_CRIT)	//3x flash
	{
#ifndef NO_SERIAL
		Serial.println("Batterie leer");
#endif
		ledOn();
		delay(250);
		ledOff();
		delay(500);
		ledOn();
		delay(250);
		ledOff();
		delay(500);
		ledOn();
		delay(250);
		ledOff();
		delay(500);
		cli();									//disable Interrupts, so nothing can wake CPU
		set_sleep_mode(SLEEP_MODE_PWR_DOWN);	
		sleep_mode();							//shutdown CPU
		
	}
	else if (volt <= BATTERY_WARN)	//2x flash
	{
#ifndef NO_SERIAL
		Serial.println("Batterie bald leer");
#endif
		ledOn();
		delay(250);
		ledOff();
		delay(500);
		ledOn();
		delay(250);
		ledOff();
		delay(500);
	}
}
#endif

/**
* @fn	void initUser()
*
* @brief	Initialisiert LED, und Button.
*
* @author	J. Boehmer
* @date	25.03.2015
*/
void initUser()
{
	pinMode(LED, OUTPUT);
	pinMode(BUTTON, INPUT_PULLUP);
}

#pragma endregion


/**
* @fn	ISR(TIMER_OVF_vect)
*
* @brief Interruptroutine f�r Tastenentprellung und Flankenerkennung. Wird alle 64us von Timer2 aufgerufen, kann aber nicht manuell ausgef�hrt werden. 
*
* @author	J. Boehmer
* @date	26.04.15
*
*/

ISR(TIMER2_OVF_vect) //will be called all 64us
{
	cli();
	static uint8_t time = 0;
	static unsigned long lasttime = millis();
	static uint16_t counter = 0;

	if (time > 16) //true every 1ms
	{	
		time = 0;
		counter++;
		if (counter >= 20000)
		{
			if (((millis() - lasttime) > (1000UL * 60UL * 45UL))||readVcc()<BATTERY_CRIT)   //after a half hour		
			{
				digitalWrite(LED, LOW);
				digitalWrite(L1, LOW);
				digitalWrite(L2, LOW);
				digitalWrite(R1, LOW);
				digitalWrite(R2, LOW);

				cli();									//disable Interrupts, so nothing can wake CPU
				set_sleep_mode(SLEEP_MODE_PWR_DOWN);
				sleep_mode();
			}
		}
	}
	//User interrupt function
	#ifdef USER_INT_FUNC
		int_func();
	#endif

	time++;
	sei();
}
#pragma region Tools

/**
 * @fn	void sleep(uint8_t s)
 *
 * @brief	Wartet fuer s Sekunden und laesst dabei die LED blinken.
 *
 * @author	J. Boehmer
 * @date	25.03.2015
 *
 * @param	s	Die Wartezeit in Sekunden.
 */

void sleep(uint8_t s)
{
	unsigned long lasttime = millis();
	while ((millis() - lasttime) > s * 1000)
	{
		ledOn();
		delay(500);
		ledOff();
		delay(250);
	}
}

/**
 * @fn	long readVcc()
 *
 * @brief	Gibt die Betriebsspannung in mV zurueck
 *
 * @author	J. Boehmer
 * @date	31.03.2015
 *
 * @return	Die Betriebsspannung in mV.
 */

long readVcc() {
	long result;
	// Read 1.1V reference against AVcc
	ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
	delay(2);							// Wait for Vref to settle
	ADCSRA |= _BV(ADSC);				// Convert
	while (bit_is_set(ADCSRA, ADSC));
	result = ADCL;
	result |= ADCH << 8;
	result = 1126400L / result;			// Back-calculate AVcc in mV
	result = (result * cal_volt) / 100;
	return result;
}

/**
 * @fn	uint8_t getSN()
 *
 * @brief	Ermittelt die Seriennummer.
 *
 * @author	J. Boehmer
 * @date	25.03.2015
 *
 * @return	Die Seriennummer (0-255)(255: nicht gesetzt, 0: reserviert).
 */

uint8_t getSN()
{
	return eeprom_read_byte((uint8_t *)ADDR_ID);
}

/**
 * @fn	void setSN(uint8_t sn)
 *
 * @brief	Schreibt die Seriennummer (in das letzte Byte des EEPROMs).
 *
 * @author	J. Boehmer
 * @date	25.03.2015
 *
 * @param	sn	Die zu schreibende Seriennummer (0-255).
 */

void setSN(uint8_t sn)
{
	return eeprom_write_byte((uint8_t *)ADDR_ID, sn);
}

void loadCal()
{
	cal_volt = eeprom_read_byte((uint8_t *)ADDR_VOLT_CAL);
	if (cal_volt == 0xFF)	//if EEPROM value not set
	{
		cal_volt = 100;		//set calibration to 1
	}
}

void setCal(uint8_t cal_byte)
{
	cal_volt = cal_byte;
	eeprom_write_byte((uint8_t *)ADDR_VOLT_CAL, cal_byte);
}

uint8_t getCal()
{
	return cal_volt;
}

long getTemp()
{
	unsigned int wADC;
	double t;

	// The internal temperature has to be used
	// with the internal reference of 1.1V.
	// Channel 8 can not be selected with
	// the analogRead function yet.

	// Set the internal reference and mux.
	ADMUX = (_BV(REFS1) | _BV(REFS0) | _BV(MUX3));
	ADCSRA |= _BV(ADEN);  // enable the ADC

	delay(20);            // wait for voltages to become stable.

	ADCSRA |= _BV(ADSC);  // Start the ADC

	// Detect end-of-conversion
	while (bit_is_set(ADCSRA, ADSC));

	// Reading register "ADCW" takes care of how to read ADCL and ADCH.
	wADC = ADCW;

	// The offset of 324.31 could be wrong. It is just an indication.
	t = (wADC - 324.31) / 1.22;

	// The returned temperature is in degrees Celcius.
	return t * 1000;
}


#pragma endregion

#pragma region Motors
/**
* @fn	void motoInit()
*
* @brief	Initialisiert die Motoren.
*
* @author	J. Boehmer
* @date	31.03.2015
*/

void motoInit()
{
	pinMode(L1, OUTPUT);
	pinMode(L2, OUTPUT);
	pinMode(R1, OUTPUT);
	pinMode(R2, OUTPUT);
}

/**
* @fn	void motoL(int16_t speed)
*
* @brief	Setzt die Geschwindigkeit des linken Motors.
* 			+255: max. Geschwindigkeit vorwaerts
* 			-255: max. Geschwindigkeit rueckwaerts
*
* @author	J. Boehmer
* @date	30.03.2015
*
* @param	speed	Geschwindigkeit des linken Motors.
*/

void motoL(int16_t speed)
{
#ifdef INVERT_L
	speed = -speed;
#endif
	if (speed == 0)
	{
		digitalWrite(L1, LOW);
		digitalWrite(L2, LOW);
	}
	else if (speed > 0)
	{
		digitalWrite(L1, LOW);
		analogWrite(L2, (uint8_t)abs(speed));
	}
	else if (speed < 0)
	{
		digitalWrite(L1, HIGH);
		analogWrite(L2, 255 - (uint8_t)abs(speed));
	}

}

/**
* @fn	void motoR(int16_t speed)
*
* @brief	Setzt die Geschwindigkeit des rechten Motors.
* 			+255: max. Geschwindigkeit vorwaerts
* 			-255: max. Geschwindigkeit rueckwaerts
*
* @author	J. Boehmer
* @date	30.03.2015
*
* @param	speed	Die Geschwindigkeit f�r den rechten Motor.
*/

void motoR(int16_t speed)
{
#ifdef INVERT_R
	speed = -speed;
#endif
	if (speed == 0)
	{
		digitalWrite(R1, LOW);
		digitalWrite(R2, LOW);
	}
	else if (speed < 0)
	{
		digitalWrite(R2, HIGH);
		analogWrite(R1, 255 - (uint8_t)abs(speed));
	}
	else if (speed > 0)
	{
		digitalWrite(R2, LOW);
		analogWrite(R1, (uint8_t)abs(speed));
	}
}

void moto(int16_t left, int16_t right)
{
	motoL(left); 
	motoR(right);
}

#pragma endregion 

#pragma region Button

/**
* @fn	uint8_t getBtn()
*
* @brief Gibt 0 oder 1 zurueck. Wenn der Wert vorher 0 war, wird nach einem Tasterdruck 1 zurueckgegeben und umgekehrt.
*
* @author	J. Boehmer
* @date	26.04.15
*
*/
uint8_t getBtn()
{
	btn_min = 0;
	btn_max = 1;
	if (btn_val > btn_max)
	{
		btn_val = 0;
	}
	return btn_val;
}

/**
* @fn	uint8_t getBtn(uint8_t max)
*
* @brief Gibt eine Zahl zwischen 0 und max zurueck. Diese wird mit jedem Tasterdruck um 1 erhoeht. Bei erreichen des Maximalwertes wird auf 0 zurueckgesetzt.
*
* @param max Der Maximalwert der zurueckgegebenen Zahl.
* @author	J. Boehmer
* @date	26.04.15
*
*/
uint8_t getBtn(uint8_t max)
{
	btn_max = max;
	btn_min = 0;
	if (btn_val > btn_max)
	{
		btn_val = 0;
	}
	return btn_val;
}

void btnHandler()
{
	
	static unsigned long lasttime = millis();
	static uint8_t value = 0;
	if (value == 0)
	{
		lasttime = millis();	
		value = 1;
	}
	else if (value > 0&&(millis()-lasttime)>=DEBOUNCE_TIME)
	{
		value = 0;
		btn_val++;
		if (btn_val > btn_max)
		{
			btn_val = btn_min;
		}
	}
}

/**
* @fn	uint8_t getBtn(uint8_t max, uint8_t min)
*
* @brief Gibt eine Zahl zwischen min und max zurueck. Diese wird mit jedem Tasterdruck um 1 erhoeht. Bei erreichen des Maximalwertes wird auf min zurueckgesetzt.
*
* @param max Der Maximalwert der zurueckgegebenen Zahl.
* @param min Der Minimalwert der zurueckgegebenen Zahl.
* @author	J. Boehmer
* @date	26.04.15
*
*/
uint8_t getBtn(uint8_t min, uint8_t max)
{
	btn_min = min;
	btn_max = max;
	if (btn_val > btn_max)
	{
		btn_val = btn_min;
	}
	return btn_val;
}

void clearBtn()
{
	btn_val = btn_min;
	
}

#pragma endregion 


#pragma region LED
/**
* @fn	int ledRead()
*
* @brief Gibt die Umgebungshelligkeit zur�ck. LED muss daf�r aus sein.
* @return Die Helligkeit als Wert zw. 0 (geringste Helligkeit) und 1024 (groesste Helligkeit)
* @author J. Boehmer
* @date	05.07
*
*/
int ledRead()
{
	uint8_t backup = ADMUX;
	analogReference(INTERNAL);
	int tmp = analogRead(LED);
	ADMUX = backup;
	return tmp;
}

void ledOn()
{
	digitalWrite(LED, HIGH);
}

void ledOff()
{
	digitalWrite(LED, LOW);
}

void ledSet(uint8_t state)
{
	digitalWrite(LED, state);
}

#pragma endregion

#pragma region line

void initLine()
{
	loadThreshold();
}

int lineRRaw()
{
	Serial.print("Line_r: ");
	Serial.println(analogRead(LINE_R));
	return analogRead(LINE_R);
}

int lineCRaw()
{
	Serial.print("Line_c: ");
	Serial.println(analogRead(LINE_C));
	return analogRead(LINE_C);
}

int lineLRaw()
{
	Serial.print("Line_l: ");
	Serial.println(analogRead(LINE_L));
	return analogRead(LINE_L);
}

void setThreshold(uint16_t left, uint16_t center, uint16_t right)
{
	thr_left = left;
	thr_center = center;
	thr_right = right;
	eeprom_write_word((uint16_t *) ADDR_THR_L, thr_left);
	eeprom_write_word((uint16_t *) ADDR_THR_R, thr_right);
	eeprom_write_word((uint16_t *) ADDR_THR_C, thr_center);
}

void loadThreshold()
{
	thr_left = eeprom_read_word((uint16_t *)ADDR_THR_L);
	thr_center = eeprom_read_word((uint16_t *)ADDR_THR_C);
	thr_right = eeprom_read_word((uint16_t *)ADDR_THR_R);

	if (thr_left == 0xFFFF)
	{
		thr_left = 512;
	}

	if (thr_center == 0xFFFF)
	{
		thr_center = 512;
	}

	if (thr_right == 0xFFFF)
	{
		thr_right = 512;
	}

}

uint8_t getLine()
{
	uint8_t tmp = 0;
	if (analogRead(LINE_L)<=thr_left)
	{
		tmp = tmp | LEFT;
	}
	if (analogRead(LINE_C)<=thr_center)
	{
		tmp = tmp | CENTER;
	}
	if (analogRead(LINE_R)<=thr_right)
	{
		tmp = tmp | RIGHT;
	}
	return tmp;
}

bool lineL()
{
	return (analogRead(LINE_L) <= thr_left);
}

bool lineC()
{
	return (analogRead(LINE_C) <= thr_center);
}

bool lineR()
{
	return (analogRead(LINE_L) <= thr_left);
}

#pragma endregion 

void playMelody(const int* melody,const int* rhythm)
{
	Serial.println(*melody);
	Serial.println(*rhythm);
	while (*melody != STOP)
	{
		if (*melody == PAUSE)
		{
			delay(*rhythm+1);
		}
		else
		{
			tone(L2, *melody, *rhythm);
		}
		
		melody++;
		rhythm++;
	}
}
