

#ifndef _CWSBOT_h
#define _CWSBOT_h

#include <Arduino.h>
#include <avr/eeprom.h>
#include <avr/sleep.h>
#undef LED_BUILTIN

#pragma region SystemDefines
//System defines
#define ADDR_ID			E2END		//EEPROM Address of the Device ID
#define ADDR_VOLT_CAL	(E2END -1)	//EEPROM Address of Voltage calibration byte

#define ADDR_THR_L		E2END -3
#define ADDR_THR_C		E2END -5
#define ADDR_THR_R		E2END -7

#define BAUD			19200

#define DEBOUNCE_TIME	25

#define BATTERY_WARN	3500
#define BATTERY_CRIT	3200
//Others
#ifndef SLEEPTIME
#define SLEEPTIME		3
#endif
#pragma endregion

#pragma region Pinsymbols
//Pin defines
//User
#define LED				A0			//User LED
#define BUTTON			3			//User Button (INT1)
#define LED_BUILTIN		LED

//RF-Module
#define MOSI			11
#define MISO			12
#define SCK				13
#define CE				10
#define CS				2

//Motors
#define L1				8		//dir left
#define L2				9		//PWM left

#define R1				5		//PWM right
#define R2				4		//dir right


//Line sensors
#define LINE_L			A1
#define LINE_C			A2
#define LINE_R			A3

//USB
#define Dp				2		//D+
#define Dm				7		//D-
#define BOOT			6		//Bootloader jumper

//UART
#define RX				0
#define TX				1

//I2C
#define SDA				A4
#define	SCL				A5

#pragma endregion


//Functions
#pragma region Motor
//Motors
void motoInit();
void motoL(int16_t speed);
void motoR(int16_t speed);
void moto(int16_t left, int16_t right);
#pragma endregion

#pragma region Button
//Button Functions
uint8_t getBtn();
uint8_t getBtn(uint8_t max);
uint8_t getBtn(uint8_t min, uint8_t max);
#define btnPressed() !digitalRead(BUTTON)
void clearBtn();
void btnHandler();
#pragma endregion

#pragma region LED
//LED-Functions
void ledOn();						//turns User LED on
void ledOff();						//turns User LED off
void ledSet(uint8_t state);					//sets LED to state	uin LED_READ
int	ledRead();
#pragma endregion

#pragma region Inits
//Other
void initAll();
#ifndef NO_USER
void initUser();
#endif
#ifndef NO_RF
void initRf();
#endif
#pragma endregion

#pragma region Helpers
void sleep(uint8_t s);
uint8_t getSN();
void setSN(uint8_t sn);
void loadCal();
void setCal(uint8_t cal_byte);
uint8_t getCal();
long readVcc();
long getTemp();

#ifndef NO_BATT_CHECK
void checkBatt();
#endif
#pragma endregion

#pragma region Line
//Line sensors functions
#ifndef NO_LINE 
void initLine();
int lineLRaw();
int lineCRaw();
int lineRRaw();
void setThreshold(uint16_t left, uint16_t common, uint16_t right);
void loadThreshold();
uint8_t getLine();
bool lineR();
bool lineC();
bool lineL();
extern uint16_t thr_center;
extern uint16_t thr_left;
extern uint16_t thr_right;

//Line symbols
#define LEFT			1
#define CENTER			2
#define RIGHT			4

#endif
#pragma endregion


#ifdef USER_INT_FUNC
void int_func()
#endif

#endif


#pragma region melody

void playMelody(const int* melody,const int* rhythm);

#define PAUSE	 0
#define STOP	 0xFFFF
#define NOTE_B0  31
#define NOTE_C1  33
#define NOTE_CS1 35
#define NOTE_D1  37
#define NOTE_DS1 39
#define NOTE_E1  41
#define NOTE_F1  44
#define NOTE_FS1 46
#define NOTE_G1  49
#define NOTE_GS1 52
#define NOTE_A1  55
#define NOTE_AS1 58
#define NOTE_B1  62
#define NOTE_C2  65
#define NOTE_CS2 69
#define NOTE_D2  73
#define NOTE_DS2 78
#define NOTE_E2  82
#define NOTE_F2  87
#define NOTE_FS2 93
#define NOTE_G2  98
#define NOTE_GS2 104
#define NOTE_A2  110
#define NOTE_AS2 117
#define NOTE_B2  123
#define NOTE_C3  131
#define NOTE_CS3 139
#define NOTE_D3  147
#define NOTE_DS3 156
#define NOTE_E3  165
#define NOTE_F3  175
#define NOTE_FS3 185
#define NOTE_G3  196
#define NOTE_GS3 208
#define NOTE_A3  220
#define NOTE_AS3 233
#define NOTE_B3  247
#define NOTE_C4  262
#define NOTE_CS4 277
#define NOTE_D4  294
#define NOTE_DS4 311
#define NOTE_E4  330
#define NOTE_F4  349
#define NOTE_FS4 370
#define NOTE_G4  392
#define NOTE_GS4 415
#define NOTE_A4  440
#define NOTE_AS4 466
#define NOTE_B4  494
#define NOTE_C5  523
#define NOTE_CS5 554
#define NOTE_D5  587
#define NOTE_DS5 622
#define NOTE_E5  659
#define NOTE_F5  698
#define NOTE_FS5 740
#define NOTE_G5  784
#define NOTE_GS5 831
#define NOTE_A5  880
#define NOTE_AS5 932
#define NOTE_B5  988
#define NOTE_C6  1047
#define NOTE_CS6 1109
#define NOTE_D6  1175
#define NOTE_DS6 1245
#define NOTE_E6  1319
#define NOTE_F6  1397
#define NOTE_FS6 1480
#define NOTE_G6  1568
#define NOTE_GS6 1661
#define NOTE_A6  1760
#define NOTE_AS6 1865
#define NOTE_B6  1976
#define NOTE_C7  2093
#define NOTE_CS7 2217
#define NOTE_D7  2349
#define NOTE_DS7 2489
#define NOTE_E7  2637
#define NOTE_F7  2794
#define NOTE_FS7 2960
#define NOTE_G7  3136
#define NOTE_GS7 3322
#define NOTE_A7  3520
#define NOTE_AS7 3729
#define NOTE_B7  3951
#define NOTE_C8  4186
#define NOTE_CS8 4435
#define NOTE_D8  4699
#define NOTE_DS8 4978

#ifndef BPM
#define BPM 120    //  you can change this value changing all the others
#endif
#define H 2*Q //half 2/4
#define Q 60000/BPM //quarter 1/4 
#define E Q/2   //eighth 1/8
#define S Q/4 // sixteenth 1/16
#define W 4*Q // whole 4/4


#pragma end                           region
